// soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
// .... jawaban soal 1
daftarHewan.sort()
for (var i = 0; i < daftarHewan.length; i++) {
    var hewan = daftarHewan[i]
    console.log (hewan)
}

// soal 2
// .... jawaban soal 2
function introduce (data){
    return "Nama saya " + data.name + ", umur saya " + data.age + " tahun, alamat saya di " + data.address + ", dan saya punya hobby yaitu " + data.hobby +"!"
}
var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
var perkenalan = introduce(data)
console.log(perkenalan) 

// soal 3
// .... jawaban soal 2
var vokal = ["a", "i", "u", "e", "o"]
function hitung_huruf_vokal (str){
    let count = 0;
    for (let letter of str.toLowerCase()){
        if (vokal.includes(letter)){
            count++;
        }
    }
    return count
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")
console.log(hitung_1 , hitung_2)


//soal 4
// ... jawaban soal 4
function hitung (angka){
    return (angka-1)*2
}
// output
console.log( hitung(0) ) 
console.log( hitung(1) ) 
console.log( hitung(2) ) 
console.log( hitung(3) ) 
console.log( hitung(5) ) 