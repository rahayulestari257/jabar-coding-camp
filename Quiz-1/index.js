// soal 1
function next_date(tanggal, bulan, tahun) {
    var format = {year: 'numeric', month: 'long', day: 'numeric' };
    const tglBaru = new Date(tahun, bulan-1, tanggal+1);
    return tglBaru.toLocaleDateString("id", format);
}
// contoh input dan output
var tanggal = 29
var bulan = 2
var tahun = 2020
console.log (next_date(tanggal, bulan, tahun));
var tanggal = 28
var bulan = 2
var tahun = 2021
console.log (next_date(tanggal, bulan, tahun));
var tanggal = 31
var bulan = 12
var tahun = 2020
console.log (next_date(tanggal, bulan, tahun));