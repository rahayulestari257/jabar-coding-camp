// soal 1
var nilai = 54;
// .... jawaban soal 1
if (nilai >= 85) {
    console.log("A")
} else if (nilai >= 75 && nilai < 85) {
    console.log("B")
} else if (nilai >= 65 && nilai < 75) {
    console.log("C")
} else if (nilai >= 55 && nilai < 65) {
    console.log("D")
} else {
    console.log("E")
}

// soal 2
var tanggal = 16;
var bulan = 4;
var tahun = 2001;
// .... jawaban soal 2
switch(bulan) {
    case 1: {console.log(tanggal, "Januari", tahun) ;break;}
    case 2: {console.log(tanggal, "Februari", tahun) ;break;}
    case 3: {console.log(tanggal, "Maret", tahun) ;break;}
    case 4: {console.log(tanggal, "April", tahun) ;break;}
    case 5: {console.log(tanggal, "Mei", tahun) ;break;}
    case 6: {console.log(tanggal, "Juni", tahun) ;break;}
    case 7: {console.log(tanggal, "Juli", tahun) ;break;}
    case 8: {console.log(tanggal, "Agustus", tahun) ;break;}
    case 9: {console.log(tanggal, "September", tahun) ;break;}
    case 10: {console.log(tanggal, "Oktober", tahun) ;break;}
    case 11: {console.log(tanggal, "November", tahun) ;break;}
    case 12: {console.log(tanggal, "Desember", tahun) ;break;}
};


// soal 3
var m = 3; // nilai m bisa diganti dengan angka berapapun
// .... jawaban soal 3
var pola = '';
for (var i = 0; i < m; i++) {
    for (var j = 0; j<=i; j++){
        pola += '#';
    }
  pola += '\n'
}
console.log(pola)


// soal 4
m =10;
// .... jawaban soal 4
i = 1;
var arr = [" - I love programming", " - I love Javascript", " - I love VueJS"];
while ( i <= m) {
    if (i > 2){
        if (i % 3 == 0){
            console.log(i + arr[2])
            var pola = '';
                for (var l = 1; l<=i; l++){
                    pola += '=';
                }
            console.log(pola) 
        } else {
            j=1
            console.log(i + arr[(i-1)%3])
        }
    } else {
        console.log(i + arr[i-1])
    }
    i++;
    }