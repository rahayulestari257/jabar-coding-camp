// soal 1
const persegipanjang = (panjang, lebar) => {
    let luas = panjang*lebar
    let keliling = 2*(panjang+lebar)
    return "Luas= " + luas + ", keliling= " + keliling
}
console.log (persegipanjang(5,4))

// soal 2
const newFunction = function literal(firstName, lastName){
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(firstName + " " + lastName)
      }
    }
}
  //Driver Code 
  newFunction("William", "Imoh").fullName() 


// soal 3
const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
}
const {firstName, lastName, address, hobby} = newObject
console.log(firstName, lastName, address, hobby)

// soal 4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west,...east]
//Driver Code
console.log(combined)

// soal 5
const planet = "earth" 
const view = "glass" 
const after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`
console.log(after)