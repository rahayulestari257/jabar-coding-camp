// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";
//.... jawaban soal 1
console.log(pertama.substring(0,4),
pertama.substring(12,18),
kedua.substring(0,7),
kedua.substring(8,18).toUpperCase())

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";
// .... jawaban soal 2
var Int_1 = parseInt(kataPertama);
var Int_2 = parseInt(kataKedua);
var Int_3 = parseInt(kataKetiga);
var Int_4 = parseInt (kataKeempat);
console.log(Int_1+Int_2*Int_3+Int_4)

// soal 3
var kalimat = 'wah javascript itu keren sekali';
// .... jawaban soal 3
var kataPertama = kalimat.substring(0,3);
var kataKedua = kalimat.substring(4,14);
var kataKetiga = kalimat.substring(15,18);
var kataKeempat = kalimat.substring(19,24);
var kataKelima = kalimat.substring(25,31);
//output soal 3
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);